import Background from "./Background";
import Layers from "./Layers";
import Text from "./Text";
import FontFamily from "./FontFamily";
import Color from "./Color";

class PanelItems {
  static Background = Background;
  static Text = Text;
  static Layers = Layers;
  static FontFamily = FontFamily;
  static Color = Color;
}

export default PanelItems;
