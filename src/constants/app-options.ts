export const panelListItems = [
  {
    id: "image",
    name: "Images",
  },
  {
    id: "text",
    name: "Text",
  },
  {
    id: "background",
    name: "Background",
  },
];

export enum PanelType {
  BACKGROUND = "Background",
}
