import Background from "./Background";
import Text from "./Text";
import Search from "./Search";
import Images from "./Images";
class Icons {
  static Background = Background;
  static Text = Text;
  static Search = Search;
  static Images = Images;
}

export default Icons;
